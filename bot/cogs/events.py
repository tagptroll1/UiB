import logging

from bot.constants import Emojis

log = logging.getLogger(__name__)

POSITIVE = [
    "\U0001f44c",  # 👌 OK HAND SIGN
    "\U0001f44d",  # 👍 THUMBS UP SIGN
    "\U00002b50",  # ⭐ WHITE MEDIUM STAR
    "\U0001f44f",  # 👏 CLAPPING HANDS SIGN
    "\U0001f37b",  # 🍻 CLINKING BEER MUGS
    "\U00002764",  # ❤ HEAVY BLACK HEART
    "\U00002665",  # ♥ BLACK HEART SUIT
    "\U00002705",  # ✅ WHITE HEAVY CHECK MARK
    "\U00002b06",  # ⬆ UPWARDS BLACK ARROW
    Emojis.correct,  # Uib discord emoji
]

NEGATIVE = [
    "\U0000274c",  # ❌ CROSS MARK
    "\U0001f595",  # 🖕 REVERSED HAND WITH MIDDLE FINGER EXTENDED
    "\U0001f44e",  # 👎 THUMBS DOWN SIGN
    "\U0001f621",  # 😡 POUTING FACE
    "\U0001f620",  # 😠 ANGRY FACE
    "\U0001f4a9",  # 💩 PILE OF POO
    "\U00002639",  # ☹ WHITE FROWNING FACE
    "\U00002b07",  # ⬇ DOWNWARDS BLACK ARROW
]


class Events:
    def __init__(self, bot):
        self.bot = bot

    async def on_raw_reaction_add(self, payload):
        if not self.bot.db:
            # Make sure we have a database connection
            return

        channel = self.bot.get_channel(payload.channel_id)
        message = await channel.get_message(payload.message_id)
        author = message.author
        guild_id = payload.guild_id

        if author.id == payload.user_id or author.bot:
            # Don't give karma to the users reaction to themself
            # Or if it's the bot, cause who cares about the bot
            return

        query = (
            """
            UPDATE users
                SET karma = karma + $1
                WHERE id = $2 AND guild_id = $3
            """
        )

        if str(payload.emoji) in POSITIVE:
            await self.bot.db.execute(query, 1, author.id, guild_id)
            log.info(f"Reaction positive, karma given to {str(author)}")

        elif str(payload.emoji) in NEGATIVE:
            await self.bot.db.execute(query, -1, author.id, guild_id)
            log.debug(f"Reaction negative, karma taken from {str(author)}")

    async def on_message(self, msg):
        pass  # Messages stuff
        # logs? points? filter?


def setup(bot):
    bot.add_cog(Events(bot))
