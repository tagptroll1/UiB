import asyncpg.pool as asyncpg
from discord import Member
import logging

from discord.ext import commands

log = logging.getLogger(__name__)


class Database:
    def __init__(self, bot) -> None:
        self.bot = bot
        self.db: asyncpg.Pool = bot.db

    async def on_ready(self) -> None:
        log.debug("on_read - database")
        await self.check_members()

    async def check_members(self) -> None:
        """
        Iterates over every member and make sure they
        have an entry in the database
        """

        log.debug("Checking all members in database")
        users = []
        for member in self.bot.get_all_members():
            users.append((member.id, member.guild.id))

        query = (
            """
            INSERT INTO users VALUES ($1, $2, 0, 0)
                ON CONFLICT(id, guild_id)
                DO NOTHING;
            """
        )
        await self.db.executemany(query, users)
        log.debug("Done checking all members")

    @commands.command()
    @commands.cooldown(1, 15, commands.BucketType.member)
    async def karma(self, ctx, member: Member = None):
        """
        Fetch a members karma from database,
        defaults to author of msg if no member is specified
        :param ctx:  Context
        :param member: check someone else's karma with this param
        """

        if not self.db:
            return await ctx.send("Sorry, I can't find my database!")

        member = member or ctx.author
        query = (
            """
            SELECT karma FROM users
                WHERE id = $1 AND guild_id = $2;
            """
        )
        row = await self.bot.db.fetchrow(query, member.id, ctx.guild.id)

        await ctx.send(f"{member} has {row['karma']} karma.")


async def check_tables(db: asyncpg.Pool) -> None:
    """
    Checks that all required tables exist or create them:
        * users
        * invites
    """

    log.debug("Creating users table, if it doesnt exist")
    await db.execute(
        """
        CREATE TABLE IF NOT EXISTS users(
            id bigint NOT NULL,
            guild_id bigint NOT NULL,
            karma int default 0,
            points int default 0,
            invited_on timestamp NOT NULL,
            CHECK (karma > -100),
            CHECK (points > -1),
            PRIMARY KEY(id, guild_id)
            );
        """
    )

    log.debug("Creating invites table, if it doesnt exist")
    await db.execute(
        """
        CREATE TABLE IF NOT EXISTS invites(
            inv_url text,
            creator_id bigint,
            guild_id bigint,
            counter int default 0,
            active boolean default True,
            PRIMARY KEY(inv_url),
            FOREIGN KEY (creator_id, guild_id) REFERENCES users(id, guild_id)
            );
        """
    )


def setup(bot):
    bot.loop.create_task(check_tables(bot.db))
    bot.add_cog(Database(bot))
    log.debug("Loaded cogs.database")
