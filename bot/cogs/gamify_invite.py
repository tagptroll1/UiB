import asyncio
import logging

from discord import Invite, NotFound, HTTPException
from discord.utils import get
from typing import List, Dict, Union

log = logging.getLogger(__name__)

BACKUP_INTERVAL = 60  # Seconds

# Alias for typecasts
InviteFreq = Dict[Union[Invite, str], int]
UserInvites = Dict[int, List[InviteFreq]]


class InviteGame:
    def __init__(self, bot):
        bot.invs: UserInvites = {}
        self.bot = bot

    async def on_ready(self):
        log.debug("Loading all invites from database")
        await self.load_invites()
        log.debug("Done loading invites.")

        log.debug("Starting backup for invites loop.")
        await self.backup_invite_status()

    async def backup_invite_status(self) -> None:
        """
        A background task which updates the database every
        minute with current invite statuses to avoid
        it being de-synced with discord.

        **
        De-sync is still possible; use "<prefix>force-sync-invite"
        to force sync all active invites, expired invites will not be
        touched, so inaccuracies will, with enough time, still be plausible.
        ** TODO: Implement force-sync-invite
        """

        while True:
            await asyncio.sleep(BACKUP_INTERVAL)
            list_invites = []

            for author, invs in self.bot.invs.items():
                for inv, count in invs:
                    list_invites.append(
                        (inv.url, author, inv.guild.id, inv.uses, True)
                    )

            log.debug(f"Created list of invites, dumping to database, length {len(list_invites)}")

            query = (
                """
                INSERT INTO invites
                    VALUES ($1, $2, $3, $4, $5)
                    ON CONFLICT (inv_url)
                        DO UPDATE SET counter = $4
                """
            )
            await self.bot.db.executemany(query, list_invites)

    async def load_invites(self) -> None:
        """
        Loads all invites from database, and updates the
        internal cache of invites, sets active invites to current
        values of the invite, else the value the database held
        """

        # update db with values of api
        list_invites = []
        for guild in self.bot.guilds:
            for invite in await guild.invites():
                list_invites.append(
                    (invite.url, invite.inviter.id, invite.guild.id, invite.uses, True)
                )

        log.debug(f"Load_invites: Created list of invites, dumping to database, length {len(list_invites)}")

        query = (
            """
            INSERT INTO invites
                VALUES ($1, $2, $3, $4, $5)
                ON CONFLICT (inv_url)
                    DO UPDATE SET counter = $4;
            """
        )
        await self.bot.db.executemany(query, list_invites)

        rows = await self.bot.db.fetch("SELECT * FROM invites")
        for inv_url, creator, _, counter, active in rows:
            try:
                invite = await self.bot.get_invite(inv_url)
                count = invite.uses
            except (NotFound or HTTPException):
                invite = inv_url
                count = counter

            if creator in self.bot.invs:
                if invite in self.bot.invs[creator]:
                    self.bot.invs[creator][invite] = count
                else:
                    self.bot.invs[creator].append({invite: count})
            else:
                self.bot.invs[creator] = [{invite: count}]

    async def update_used_invite(self, guild):
        query = (
            """
            SELECT * FROM invites
                WHERE guild_id = $1
                AND active = TRUE
                AND counter >= 1;
            """
        )
        db = self.bot.db
        invites = db.fetchall(query, guild.id)

        for row in invites:
            invite = get(guild.invites, url=row["inv_url"])

            if not row:
                query_update = (
                    """
                    UPDATE invites
                        SET active = FALSE
                        WHERE inv_url = $2;
                    """
                )
                db.execute(query_update, row["inv_url"])
                continue

            if invite.uses > row["counter"]:
                query_update = (
                    """
                    UPDATE invites
                        SET counter = $1
                        WHERE inv_url = $3;
                    """
                )
                db.execute(query_update, row["inv_url"])
                return invite

    async def on_member_join(self, member):
        await self.update_used_invite(member.guild)


def setup(bot):
    bot.add_cog(InviteGame(bot))
