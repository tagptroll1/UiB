# Most of the credit for this is owed to Python discords bot
# https://gitlab.com/python-discord/projects/bot/blob/master/bot/constants.py

import os
import logging
import yaml

from collections.abc import Mapping
from pathlib import Path
from typing import List
from yaml.constructor import ConstructorError


log = logging.getLogger(__name__)


def _env_var_constructor(loader, node):
    """
    Implements a custom YAML tag for loading optional environment
    variables. If the environment variable is set, returns the
    value of it. Otherwise, returns `None`.
    *Credits to Python discord*

    Example usage in the YAML configuration:

        # Optional app configuration. Set `MY_APP_KEY` in
        # the environment to use it.
        application:
            key: !ENV 'MY_APP_KEY'
    """

    default = None

    try:
        # Try to construct a list from this YAML node
        value = loader.construct_sequence(node)

        if len(value) >= 2:
            # If we have at least two values, then we have both
            #  a key and a default value
            default = value[1]
            key = value[0]
        else:
            # Otherwise, we just have a key
            key = value[0]
    except ConstructorError:
        # This YAML node is a plain value rather than a list,
        #  so we just have a key
        value = loader.construct_scalar(node)

        key = str(value)

    return os.getenv(key, default)


yaml.SafeLoader.add_constructor("!ENV", _env_var_constructor)


with open("../config-default.yml", encoding="UTF-8") as f:
    _CONFIG_YAML = yaml.safe_load(f)


def _recursive_update(original, new):
    """
    Helper method which implements a recursive `dict.update`
    method, used for updating the original configuration with
    configuration specified by the user.
    *credits python discord*
    """

    for key, value in original.items():
        if key not in new:
            continue

        if isinstance(value, Mapping):
            if not any(
                    isinstance(sub_value, Mapping)
                    for sub_value
                    in value.values()
            ):
                original[key].update(new[key])
            _recursive_update(original[key], new[key])
        else:
            original[key] = new[key]


if Path("../config.yml").exists():
    with open("../config.yml", encoding="UTF-8") as f:
        user_config = yaml.safe_load(f)
    _recursive_update(_CONFIG_YAML, user_config)


class YAMLGetter(type):
    """
    Implements a custom metaclass used for accessing
    configuration data by simply accessing class attributes.
    Supports getting configuration from up to two levels
    of nested configuration through `section` and `subsection`.

    `section` specifies the YAML configuration section (or "key")
    in which the configuration lives, and must be set.

    `subsection` is an optional attribute specifying the section
    within the section from which configuration should be loaded.

    Example Usage:

        # config.yml
        bot:
            prefixes:
                direct_message: ''
                guild: '!'

        # config.py
        class Prefixes(metaclass=YAMLGetter):
            section = "bot"
            subsection = "prefixes"

        # Usage in Python code
        from config import Prefixes
        def get_prefix(bot, message):
            if isinstance(message.channel, PrivateChannel):
                return Prefixes.direct_message
            return Prefixes.guild
    """

    subsection = None

    def __getattr__(cls, name):
        name = name.lower()

        try:
            if cls.subsection is not None:
                return _CONFIG_YAML[cls.section][cls.subsection][name]
            return _CONFIG_YAML[cls.section][name]
        except KeyError:
            dotted_path = '.'.join(
                (cls.section, cls.subsection, name)
                if cls.subsection is not None else (cls.section, name)
            )
            log.critical(
                f"Tried accessing configuration variable at `{dotted_path}`,"
                " but it could not be found.")
            raise

    def __getitem__(cls, name):
        return cls.__getattr__(name)


class Bot(metaclass=YAMLGetter):
    section = "bot"

    token: str


class Filter(metaclass=YAMLGetter):
    section = "filter"

    domain_blacklist: List[str]
    word_watchlist: List[str]
    token_watchlist: List[str]

    channel_whitelist: List[int]
    role_whitelist: List[int]


class Colors(metaclass=YAMLGetter):
    section = "style"
    subsection = "colors"

    soft_red: int
    soft_green: int


class Emojis(metaclass=YAMLGetter):
    section = "style"
    subsection = "emojis"

    # UiB server
    correct: str

    # Test server
    status_online: str
    status_offline: str
    status_dnd: str
    status_away: str
    warning_sign: str
    alert_sign: str
    info_sign: str


class Icons(metaclass=YAMLGetter):
    section = "style"
    subsection = "icons"

    logo: str


class Guild(metaclass=YAMLGetter):
    section = "guild"

    id: int
    testid: int


class Channels(metaclass=YAMLGetter):
    section = "guild"
    subsection = "channels"

    dev: int


class Roles(metaclass=YAMLGetter):
    section = "guild"
    subsection = "roles"

    admin: int
    moderator: int
    ledere: int

    # test server
    dev: int


class Keys(metaclass=YAMLGetter):
    section = "keys"

    wolfram: str
    uibkey: str


class Urls(metaclass=YAMLGetter):
    section = "urls"

    uib: str
    mitt_uib: str
    student_web: str
    web_mail: str


class Database(metaclass=YAMLGetter):
    section = "database"

    username: str
    password: str


BOT_DIR = os.path.dirname(__file__)
PROJECT_ROOT = os.path.abspath(os.path.join(BOT_DIR, os.pardir))
