import asyncio
import logging
import asyncpg
import socket
import sys

from aiohttp import ClientSession, TCPConnector, AsyncResolver
from discord import ClientException, LoginFailure, Game
from discord.ext.commands import AutoShardedBot, when_mentioned_or
from pathlib import Path

from bot.constants import Bot as BotConfig
from bot.constants import Database


log = logging.getLogger(__name__)


class Uib(AutoShardedBot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.db = kwargs.get("database", None)

        self.http_session = ClientSession(
            connector=TCPConnector(
                resolver=AsyncResolver(),
                family=socket.AF_INET
            )
        )
        self.loop.create_task(self.load_extensions())

    async def load_extensions(self):
        # .py files that are not cogs in the cogs directory
        ignored_files = set()

        if not self.db:
            ignored_files.add("database")

        cogs = Path("./cogs")
        for cog in cogs.iterdir():
            if cog.is_dir():
                continue

            elif cog.stem in ignored_files:
                continue

            if cog.suffix == ".py":
                path = ".".join(cog.with_suffix("").parts)
                try:
                    self.load_extension(path)
                    log.info(f"Loading cog {path}")

                except ImportError:
                    log.exception(f"Failed to load: {path}")

                except ClientException:
                    log.exception(
                        f"Cog does not have a setup function: {path}"
                    )

    async def on_ready(self):
        log.info("Bot ready")

    async def logout(self):
        """
        Close dbs, connections etc then log out
        """

        log.info("Bot logging out...")
        await self.http_session.close()
        log.info("Http session closed")

        if self.db:
            await self.db.close()
            log.info("closed database pools")

        await super().logout()


def run():

    if BotConfig.token == "TOKEN":
        log.critical("Token unchanged in config.yml!")
        sys.exit(1)

    loop = asyncio.get_event_loop()

    credentials = {
        "user": Database.username,
        "password": Database.password,
        "database": "uib", "host": "localhost"
    }

    pool = loop.run_until_complete(asyncpg.create_pool(**credentials))

    uib = Uib(
        when_mentioned_or("!"),
        activity=Game("with books"),
        case_insensitive=True,
        max_messages=10000,
        database=pool
    )

    try:
        # Start the event loop with bot.start
        loop.run_until_complete(uib.start(BotConfig.token))

    except LoginFailure:
        # if it fails to login, terminate the bot and terminal
        log.critical("Missing, or invalid token!")
        loop.run_until_complete(uib.logout())
        sys.exit(1)

    except KeyboardInterrupt:
        # Most safe way to shut down is with a KeyboardInterrupt, or a command
        log.info("Bot was forcefully closed with KeyboardInterrupt")
        loop.run_until_complete(uib.logout())
        sys.exit(1)


if __name__ == "__main__":
    run()
