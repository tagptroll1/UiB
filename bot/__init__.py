import logging
import os
import sys

DEBUG = True if "true" in os.environ.get("DEBUG", "true").lower() else False

if DEBUG:
    logging_handlers = [logging.StreamHandler(stream=sys.stdout)]

else:
    logging_handlers = []

logging.basicConfig(
    format="%(asctime)s Bot: | %(name)30s | %(levelname)8s | %(message)s",
    datefmt="%b %d %H:%M:%S",
    level=logging.DEBUG if DEBUG else logging.INFO,
    handlers=logging_handlers
)

for key, value in logging.Logger.manager.loggerDict.items():
    # Force all existing loggers to the correct level and handlers
    # This happens long before we instantiate our loggers, so
    # those should still have the expected level

    if key == "bot":
        continue

    if not isinstance(value, logging.Logger):
        # There might be some logging.PlaceHolder objects in there
        continue

    if DEBUG:
        value.setLevel(logging.DEBUG)
    else:
        value.setLevel(logging.INFO)

    for handler in value.handlers.copy():
        value.removeHandler(handler)

    for handler in logging_handlers:
        value.addHandler(handler)

logging.getLogger("discord.client").setLevel(logging.ERROR)
logging.getLogger("discord.gateway").setLevel(logging.ERROR)
logging.getLogger("discord.state").setLevel(logging.ERROR)
logging.getLogger("discord.http").setLevel(logging.ERROR)
logging.getLogger("websockets.protocol").setLevel(logging.ERROR)
